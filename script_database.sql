DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS rented CASCADE;
DROP TABLE IF EXISTS rented_movie CASCADE;
DROP TABLE IF EXISTS movie CASCADE;
DROP TABLE IF EXISTS director CASCADE;
DROP TABLE IF EXISTS store CASCADE;

CREATE TABLE customer (
	id SERIAL NOT NULL,
	name VARCHAR(50) NOT NULL,
	password VARCHAR(60) NOT NULL,
	email VARCHAR(60) UNIQUE NOT NULL,
	CONSTRAINT pk_customer
	PRIMARY KEY (id)
);

CREATE TABLE rented (
	id SERIAL NOT NULL,
  customer_id INTEGER NOT NULL,
  date_rented DATE NOT NULL,
	date_devolution DATE,
	CONSTRAINT pk_rented
	PRIMARY KEY(id)
);

CREATE TABLE rented_movie (
	rented_id INTEGER NOT NULL,
  movie_id INTEGER NOT NULL
);

CREATE TABLE movie (
	id SERIAL NOT NULL,
  title VARCHAR(100) NOT NULL,
  director_id INTEGER NOT NULL,
  CONSTRAINT pk_movie
  PRIMARY KEY(id)
);

CREATE TABLE director (
	id SERIAL NOT NULL,
  name VARCHAR(100) NOT NULL,
  CONSTRAINT pk_director
  PRIMARY KEY(id)
);

CREATE TABLE store (
	id SERIAL NOT NULL,
  movie_id INTEGER NOT NULL,
  amount INTEGER NOT NULL
);

ALTER TABLE movie
	ADD CONSTRAINT fk_director_movie
	FOREIGN KEY(director_id)
	REFERENCES director(id)
	ON UPDATE CASCADE;
 
ALTER TABLE rented_movie
	ADD CONSTRAINT fk_rentedmovie_rented
	FOREIGN KEY(rented_id)
	REFERENCES rented(id)
	ON UPDATE CASCADE,
  ADD CONSTRAINT fk_rentedmovie_movie
	FOREIGN KEY(movie_id)
	REFERENCES movie(id)
	ON UPDATE CASCADE;

ALTER TABLE rented
	ADD CONSTRAINT fk_customer_rented
	FOREIGN KEY(customer_id)
	REFERENCES customer(id)
	ON UPDATE CASCADE;
  
ALTER TABLE store
	ADD CONSTRAINT fk_rented_movie
	FOREIGN KEY(movie_id)
	REFERENCES movie(id)
	ON UPDATE CASCADE;

CREATE VIEW movies_available
AS
SELECT m.id, m.title, d.name AS director, (s.amount - COUNT(m.*)) AS amount
FROM rented_movie rm
INNER JOIN rented r
ON rm.rented_id = r.id
INNER JOIN movie m
ON rm.movie_id = m.id
INNER JOIN store s
ON m.id = s.movie_id
INNER JOIN director d
ON d.id = m.director_id
WHERE r.date_devolution IS NULL
GROUP BY m.id, s.amount, d.name
ORDER BY m.id ASC;

CREATE FUNCTION rent_movie(customer_id BIGINT, movie_id BIGINT)
	RETURNS VOID
  LANGUAGE plpgsql
AS
$$
DECLARE 
	last_id INTEGER; 
	movie_count INTEGER;
BEGIN
  SELECT amount INTO movie_count FROM  movies_available WHERE id = movie_id;
  
  IF movie_count = 0 THEN
    RAISE EXCEPTION 'Todos os exemplares deste filme estão alugados';
  END IF;
  
  INSERT INTO rented(customer_id, date_rented) VALUES (customer_id, NOW()) RETURNING id INTO last_id;
  INSERT INTO rented_movie(rented_id, movie_id) VALUES (last_id, movie_id);
END;
$$

INSERT INTO director(name) VALUES ('Mel Gibson');
INSERT INTO director(name) VALUES ('Joss Whedon');
INSERT INTO director(name) VALUES ('Antoine Fuqua');

INSERT INTO movie(title, director_id) VALUES ('Até o Último Homem', 1);
INSERT INTO movie(title, director_id) VALUES ('Os Vingadores', 2);
INSERT INTO movie(title, director_id) VALUES ('Lágrimas do Sol', 3);

INSERT INTO store(movie_id, amount) VALUES (1, 10); 
INSERT INTO store(movie_id, amount) VALUES (2, 15);
INSERT INTO store(movie_id, amount) VALUES (3, 12);

-- Senha -> 123456
INSERT INTO customer(name, password, email) VALUES ('João Santos', '$2a$08$FgzqoMEywK76N3WvoSiYruJ7jLDTTqF7M.xmScbc5YaYmtQk2yeHO', 'joao@gmail.com');
INSERT INTO customer(name, password, email) VALUES ('Paulo Santos', '$2a$08$FgzqoMEywK76N3WvoSiYruJ7jLDTTqF7M.xmScbc5YaYmtQk2yeHO', 'paulo@gmail.com');
INSERT INTO customer(name, password, email) VALUES ('Maria Santos', '$2a$08$FgzqoMEywK76N3WvoSiYruJ7jLDTTqF7M.xmScbc5YaYmtQk2yeHO', 'maria@gmail.com');
INSERT INTO customer(name, password, email) VALUES ('Marta Santos', '$2a$08$FgzqoMEywK76N3WvoSiYruJ7jLDTTqF7M.xmScbc5YaYmtQk2yeHO', 'marta@gmail.com');

INSERT INTO rented(date_rented, date_devolution, customer_id) VALUES ('2019-03-13', '2019-04-03', 1);
INSERT INTO rented(date_rented, customer_id) VALUES ('2019-04-21', 2);
INSERT INTO rented(date_rented, customer_id) VALUES ('2019-05-22', 3);
INSERT INTO rented(date_rented, customer_id) VALUES ('2019-06-23', 4);

INSERT INTO rented_movie(rented_id, movie_id) VALUES (1, 1);
INSERT INTO rented_movie(rented_id, movie_id) VALUES (2, 1);
INSERT INTO rented_movie(rented_id, movie_id) VALUES (3, 2);
INSERT INTO rented_movie(rented_id, movie_id) VALUES (4, 3);

