# Desafio backend

## Descrição do Projeto
Esse desafio tem como objetivo criar um webservice de locadora de filmes.

</br>

### Features:


- [x] Cadastro de usuário;
- [x] Login e Logoff;
- [x] Listagem de filmes disponíveis;
- [x] Locação de filmes;
- [x] Devolução de filmes;
- [x] Pesquisa de filme por título.
- [x] Listagem de todos os emprestimos do usúario.

</br>

### Pré-requisitos

</br>

Antes de começar, você vai precisar ter instalado em sua maquina as seguintes ferramentas:

- OpenJDK 11
- Postgresql 12
- Docker (Opcional)

</br>

### Preaparando o ambiente 

</br>

Instalando OpenJDK e o Postgres 12 no Ubuntu 20.04:

```bash
# Instalando o openjdk-11
$ sudo apt install openjdk-11-jdk

# Instalando o postgres
$ sudo apt install postgresql-12

```

</br>

### Usando docker (Optional)

</br>

Caso não tenha o docker instalado em sua máquina, recomendo seguir o tutorial de como [instalar o Docker](https://docs.docker.com/engine/install/ubuntu/) no Ubuntu 20.04.

Usando o postgres no docker. (Recomendado)

```bash
# Criando e rodando um container com postgres 12
$ docker run --name=postgres -e POSTGRES_PASSWORD=123456 -p 5432:5432 -d postgres:12
```

</br>

### 🛠 Tecnologias
- OpenJDk 11
- SpringBoot 2.3
- Postgres 12

</br>

### Entregáveis
- [Script de criação do banco de dados](https://gitlab.com/jean_xavier_/desafio-backend/-/blob/develop/script_database.sql)
- [Arquivo com a especificação do webservices](https://gitlab.com/jean_xavier_/desafio-backend/-/raw/develop/specification-swagger.yaml)

Recomendo usar o [SwaggerEditor](https://editor.swagger.io/?_ga=2.233844954.515047834.1600294234-1832976527.1600294234) para obter uma visualização mais amigável da especificação. Para isso, basta copiar o conteúdo da especificação e colar no editor que encontra-se no lado esquerdo.
