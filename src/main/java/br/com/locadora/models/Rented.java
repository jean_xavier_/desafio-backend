package br.com.locadora.models;

import java.util.Date;

public class Rented {
    private Long id;
    private String customer;
    private String movie;
    private Date dateRented;
    private Date dateDevolution;

    public Rented() { }

    public Rented(Long id, String customer, String movie, Date dateRented, Date dateDevolution) {
        this.id = id;
        this.customer = customer;
        this.movie = movie;
        this.dateRented = dateRented;
        this.dateDevolution = dateDevolution;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Date getDateRented() {
        return dateRented;
    }

    public void setDateRented(Date dateRented) {
        this.dateRented = dateRented;
    }

    public Date getDateDevolution() {
        return dateDevolution;
    }

    public void setDateDevolution(Date dateDevolution) {
        this.dateDevolution = dateDevolution;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }
}
