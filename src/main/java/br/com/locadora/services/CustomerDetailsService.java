package br.com.locadora.services;

import br.com.locadora.models.Customer;
import br.com.locadora.models.CustomerDetails;
import br.com.locadora.repository.CustomerRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomerDetailsService implements UserDetailsService {
    private final CustomerRepository customerRepository;

    public CustomerDetailsService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        CustomerDetails customer = loadUserByEmail(email);
        return new User(customer.getEmail(), customer.getPassword(), new ArrayList<>());
    }

    public CustomerDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        Customer customer = customerRepository.findByEmail(email);

        if (customer == null) {
            throw new UsernameNotFoundException("Não foi encontrado nenhum usuario com o email: '" + email + "'");
        }

        return new CustomerDetails(customer.getEmail(), customer.getPassword());
    }
}
