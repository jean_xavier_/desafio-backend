package br.com.locadora.controllers;

import br.com.locadora.dto.AuthRequestDTO;
import br.com.locadora.dto.AuthResponseDTO;
import br.com.locadora.dto.FailedRequestDTO;
import br.com.locadora.models.CustomerDetails;
import br.com.locadora.services.CustomerDetailsService;
import br.com.locadora.utils.JwtUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AuthenticationController {
    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final CustomerDetailsService customerDetailsService;

    public AuthenticationController(JwtUtil jwtUtil, AuthenticationManager authenticationManager,
                                    CustomerDetailsService customerDetailsService) {
        this.jwtUtil = jwtUtil;
        this.authenticationManager = authenticationManager;
        this.customerDetailsService = customerDetailsService;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> authentication(@RequestBody AuthRequestDTO authRequestDTO) throws Exception {
        try {
            var auth = new UsernamePasswordAuthenticationToken(authRequestDTO.getEmail(), authRequestDTO.getPassword());
            authenticationManager.authenticate(auth);
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new FailedRequestDTO("Email ou senha invalida"));
        }

        final CustomerDetails customerDetails = customerDetailsService.loadUserByEmail(authRequestDTO.getEmail());
        final String jwt = jwtUtil.generateToken(customerDetails.getEmail());

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new AuthResponseDTO(jwt));
    }
}
