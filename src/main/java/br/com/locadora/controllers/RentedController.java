package br.com.locadora.controllers;

import br.com.locadora.dto.NewRentedDTO;
import br.com.locadora.models.Rented;
import br.com.locadora.repository.RentedRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RentedController {
    private final RentedRepository rentedRepository;

    public RentedController(final RentedRepository repository) {
        this.rentedRepository = repository;
    }

    @PostMapping("/rents")
    public ResponseEntity<Void> save(@Validated @RequestBody NewRentedDTO rentedDTO) {
        this.rentedRepository.save(rentedDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/rents")
    public ResponseEntity<Void> update(@Validated @RequestParam Long code) {
        this.rentedRepository.update(code);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/rents")
    public ResponseEntity<List<Rented>> list(@RequestAttribute(value = "currentCustomerEmail") String email) {
        List<Rented> renteds = this.rentedRepository.findBy(email);
        return ResponseEntity.ok(renteds);
    }
}
