package br.com.locadora.controllers;

import br.com.locadora.models.Movie;
import br.com.locadora.repository.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieController {
    private final MovieRepository movieRepository;

    public MovieController(MovieRepository repository) {
        this.movieRepository = repository;
    }

    @GetMapping("/movies")
    public ResponseEntity<List<Movie>> list() {
        List<Movie> movies = movieRepository.findAll();
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/movies/filter")
    public ResponseEntity<List<Movie>> listByFilter(@RequestParam String title) {
        List<Movie> movies = this.movieRepository.findBy(title);
        return ResponseEntity.ok(movies);
    }
}
