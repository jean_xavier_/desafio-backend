package br.com.locadora.controllers;

import br.com.locadora.dto.FailedRequestDTO;
import br.com.locadora.models.Customer;
import br.com.locadora.repository.CustomerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CustomerController {
    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;

    public CustomerController(CustomerRepository repository, PasswordEncoder passwordEncoder) {
        this.customerRepository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/customers")
    public ResponseEntity<?> save(@Validated @RequestBody Customer customer) {
        if (customerRepository.containsEmail(customer.getEmail()) == 0) {
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customer = this.customerRepository.save(customer);
            return ResponseEntity.status(HttpStatus.CREATED).body(customer);
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new FailedRequestDTO("Email já existente em nossa base de dados"));
    }

    @GetMapping("/customers/profile")
    public ResponseEntity<Customer> showProfile(@RequestAttribute(value = "currentCustomerEmail") String email) {
        Customer customer = this.customerRepository.findByEmail(email);
        return ResponseEntity.ok(customer);
    }
}
