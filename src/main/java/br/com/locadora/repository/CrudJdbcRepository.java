package br.com.locadora.repository;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public abstract class CrudJdbcRepository<T, U> {
    protected final JdbcTemplate jdbc;
    protected final Class<T> entity;

    public CrudJdbcRepository(Class<T> entity, JdbcTemplate jdbc) {
        this.jdbc = jdbc;
        this.entity = entity;
    }

    public List<T> findAll() {
        String table = entity.getSimpleName().toLowerCase();
        String sql = "SELECT * FROM " + table;
        return jdbc.query(sql, new BeanPropertyRowMapper<>(entity));
    }

    protected List<T> findAll(String sql) {
        return jdbc.query(sql, new BeanPropertyRowMapper<>(entity));
    }

    protected T findOneBy(String sql, Object... args) {
        return jdbc.queryForObject(sql, args, new BeanPropertyRowMapper<>(entity));
    }

    public List<T> findBy(String sql) {
        return jdbc.query(sql, new BeanPropertyRowMapper<>(entity));
    }

    public T findById(U id) {
        String table = entity.getSimpleName().toLowerCase();
        String sql = "SELECT * FROM " + table + " id = ?";
        return jdbc.queryForObject(sql, new Object[]{ id }, new BeanPropertyRowMapper<T>());
    }

    public Boolean save(String sql, Object... args) {
        return jdbc.update(sql, args) == 1;
    }

    public Boolean update(String sql, Object... args) {
        return jdbc.update(sql, args) == 1;
    }

    public Boolean deleteById(U id) {
        String sql = "SELECT * FROM " + entity.getSimpleName().toLowerCase() + " id = ?";
        return jdbc.update(sql, id) == 1;
    }
}
