package br.com.locadora.repository;

import br.com.locadora.models.Movie;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovieRepository extends CrudJdbcRepository<Movie, Long> {
    public MovieRepository(JdbcTemplate jdbc) {
        super(Movie.class, jdbc);
    }

    @Override
    public List<Movie> findAll() {
        String sql = "SELECT ma.id, ma.title, ma.director FROM movies_available ma WHERE ma.amount > 0";
        return super.findAll(sql);
    }

    @Override
    public List<Movie> findBy(String title) {
        if (title.contains(" ")) {
            title = title.replaceAll(" ", "|");
        }

        String sql = "SELECT ma.id, ma.title, ma.director FROM movies_available ma " +
                "WHERE ma.amount > 0 AND ma.title SIMILAR TO '%(" +  title + ")%';";
        return super.findBy(sql);
    }
}
