package br.com.locadora.repository;

import br.com.locadora.dto.NewRentedDTO;
import br.com.locadora.models.Rented;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RentedRepository extends CrudJdbcRepository<Rented, Long> {
    public RentedRepository(JdbcTemplate jdbc) {
        super(Rented.class, jdbc);
    }

    public void save(NewRentedDTO rented) {
        String sql = "SELECT rent_movie(" + rented.getCustomerID() + "," + rented.getMovieID() + ")";
        this.jdbc.execute(sql);
    }

    public void update(Long code) {
        String sql = "UPDATE rented SET date_devolution = NOW() WHERE id = ?";
        super.update(sql, code);
    }

    @Override
    public List<Rented> findBy(String email) {
        String sql = "SELECT r.id, c.name as customer, m.title as movie, r.date_rented, r.date_devolution " +
                "FROM rented r " +
                "INNER JOIN rented_movie re " +
                "ON r.id = re.rented_id " +
                "INNER JOIN movie m " +
                "ON re.movie_id = m.id " +
                "INNER JOIN customer c " +
                "ON r.customer_id = c.id " +
                "WHERE c.email = '" + email + "'";
        return super.findBy(sql);
    }
}
