package br.com.locadora.repository;

import br.com.locadora.models.Customer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository extends CrudJdbcRepository<Customer, Long> {
    public CustomerRepository(JdbcTemplate jdbc) {
        super(Customer.class, jdbc);
    }

    public Customer save(Customer customer) {
        String sql = "INSERT INTO customer (name, email, password) VALUES (?, ?, ?)";
        super.save(sql, customer.getName(), customer.getEmail(), customer.getPassword());
        return findByEmail(customer.getEmail());
    }

    public Customer findByEmail(String email) {
        String sql = "SELECT * FROM customer WHERE email = ?";
        return super.findOneBy(sql, email);
    }

    public Integer containsEmail(String email) {
        String sql = "SELECT COUNT(*) FROM customer WHERE email = '"+ email + "'";
        return super.jdbc.queryForObject(sql, Integer.class);
    }
}
