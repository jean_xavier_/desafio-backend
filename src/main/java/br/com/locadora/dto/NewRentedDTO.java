package br.com.locadora.dto;

public class NewRentedDTO {
    private Long customerID;
    private Long movieID;

    public NewRentedDTO() { }

    public NewRentedDTO(Long customerID, Long movieID) {
        this.customerID = customerID;
        this.movieID = movieID;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getMovieID() {
        return movieID;
    }

    public void setMovieID(Long movieID) {
        this.movieID = movieID;
    }
}
