package br.com.locadora.dto;

public class FailedRequestDTO {
    private final String error;

    public FailedRequestDTO(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
